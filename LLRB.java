package examPrep;

/**
 * 
 * @author Anass Chaaraoui
 * 
 * */

public class LLRB<T extends Comparable<T>> {
	
	public final static boolean RED = true;
	public final static boolean BLACK = false;
	
	Node root;
	
	public LLRB() {
		
	}
	
	public void insert(T x) {
		root = insert(x, root);
		root.color = BLACK;
	}
	
	private Node insert(T x, Node oldNode) {
		if(oldNode == null) return new Node(x);
		if(isRed(oldNode.fg) && isRed(oldNode.fd)) oldNode.scinder();
		
		int c = x.compareTo(oldNode.cle);
		if(c < 0) return new Node(oldNode.cle, insert(x, oldNode.fg), oldNode.fd, RED).balance();
		if(c > 0) return new Node(oldNode.cle, oldNode.fg, insert(x, oldNode.fd), RED).balance();
		else return oldNode;
	}
	
	public boolean isRed(Node n) {
		if(n == null) return false;
		return n.color == RED;
	}
	
	public void print(Node n) {
		if (n != null) {
			print(n.fg);
			System.out.println(n.cle);
			print(n.fd);
		}
	}
	
	class Node {
		T cle;
		Node fg, fd;
		boolean color;
		
		public Node(T c, Node ffg, Node ffd, boolean clr) {
			cle = c;
			fg = ffg;
			fd = ffd;
			color = clr;
		}
		
		public void scinder() {
			color = !color;
			fg.color = !fg.color;
			fd.color = !fd.color;
		}

		public Node(T c) {
			this(c, null, null, RED);
		}
		
		public Node RL() {
			return new Node(fd.cle, new Node(cle, fg, fd.fg, fd.color), fd.fd, color);
		}
		
		public Node RR() {
			return new Node(fg.cle, fg.fg, new Node(cle, fg.fd, fd, fg.color), color);
		}
		
		public Node balance() {
			if(!isRed(fg) && isRed(fg.fg)) return RR();
			if(isRed(fd) && !isRed(fg)) return RL().balance();
			return this;
		}
	}
	
	public static void main(String[] args) {
		LLRB<Integer> l = new LLRB<>();
		l.insert(5);
		l.insert(3);
		l.insert(10);
		l.insert(8);
		l.insert(8);
		
		l.print(l.root);
		
//		System.out.println(l.root.color);
	}
}
